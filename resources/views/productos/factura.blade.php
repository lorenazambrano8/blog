@extends('layouts.plantilla')

@section('title', 'productos factura')

@section('content')
<div class="container">
<table class="table table-success table-striped table-hover">
    <h2 class="display-4 text-center my-5">Factura</h2>
    
<thead>
    <tr>

        <th class="table-dark"><h3>Nombre</h3></th>
        <th class="table-dark"><h3>Identificacion</h3></th>
        <th class="table-dark"><h3>Direccion</h3></th>
        <th class="table-dark"><h3>Telefono</h3></th>
        <th class="table-dark"><h3>Correo</h3></th>
        <th class="table-dark"><h3></h3></th>
        <th class="table-dark"><h3></h3></th>
    </tr>
     
    <td><p>{{$cliente->nombre}}</p></td>
    <td><p>{{$cliente->cedula_ciudadania}}</p></td>
    <td><p>{{$cliente->direccion}}</p></td>
    <td><p>{{$cliente->telefono}}</p></td>
    <td><p>{{$cliente->correo}}</p></td>

</thead>

    <tr>
        
        <th class="table-dark"><h3>Nombre</h3></th>
        <th class="table-dark"><h3>Descripcion</h3></th>
        <th class="table-dark"><h3>Precio ($)</h3></th>
        <th class="table-dark"><h3>Iva (%)</h3></th>
        <th class="table-dark"><h3>Cantidad</h3></th>
        <th class="table-dark"><h3>Tipo</h3></th>
        <th class="table-dark"></th>
        
    </tr>
    
    <td><p>{{$producto->nombre}}</p></td>
    <td><p>{{$producto->descripcion}}</p></td>
    <td><p>{{$producto->precio}}</p></td>
    <td><p>{{$producto->iva}}</p></td>
    <td><p>{{$producto->cantidad}}</p></td>
    <td><p>{{$producto->tipo}}</p></td>


</tr>
 

</table>
</div> 
@endsection