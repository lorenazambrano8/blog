@extends('layouts.plantilla')

@section('title','Producto')

@section('content')
<h2 class="display-4 text-center my -5">Bienvenidos a productos:</h2>
    <a href="{{route('productos.create')}}">Registrar producto</a>
    <ul>
        @foreach ($productos as $producto)
        <li>
            <a href="{{route('productos.show', $producto->id)}}">{{$producto->nombre}}</a>
        </li>          
        @endforeach
    </ul>
@endsection
    
