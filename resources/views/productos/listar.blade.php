@extends('layouts.plantilla')

@section('title','Productos')

@section('content')
{{-- <form action="{{route('productos.create')}}"> --}}
<h2 class="display-4 text-center my-4">Lista de Productos</h2>
    <table class="table table-success table-striped table-hover">
        
        <thead>
            <tr>
                <th class="table-dark"><h4>Nombre</h4></th>
                <th class="table-dark"><h4>Descripcion</h4></th>
                <th class="table-dark"><h4>Precio</h4></th>
                <th class="table-dark"><h4>Cantidad</h4></th>
                <th class="table-dark"><h4>Fecha Vencimiento</h4></th>
                <th class="table-dark"><h4>Tipo</h4></th>
                <th class="table-dark"><h4>Iva</h4></th>
                <th class="table-dark"></th>
                <th class="table-dark"><h4>Acciones</h4></th>
                <th class="table-dark"></th>
                <th class="table-dark"></th>
                <th class="table-dark"></th>
    
            </tr>
         </thead>
         <tbody>
            @foreach ($producto as $producto)
             <tr>
                <td>{{$producto->nombre}}</td>
                <td>{{$producto->descripcion}}</td>
                <td>{{$producto->precio}}</td>
                <td>{{$producto->cantidad}}</td>
                <td>{{$producto->fecha_vencimiento}}</td>
                <td>{{$producto->tipo}}</td>
                <td>{{$producto->iva}}</td>
                 
                <td><a href="{{route('productos.show', $producto)}}"><button class="btn btn-primary mb-3" type="submit">Eliminar</button></a></td>
                <td><a href="{{route('productos.mostrar', $producto)}}"><button class="btn btn-primary mb-3" type="submit">mostrar</button></a></td>
                {{--<td><a href="{{route('productos.show', $producto)}}">Eliminar Producto</a></td>--}}
                <td><a href="{{route('productos.edit', $producto)}}"><button class="btn btn-primary mb-3" type="submit">Editar</button></a></td>
                {{--<td><a href="{{route('productos.edit', $producto)}}">Editar Producto</a></td>--}}
                <td><a href="{{route('productos.create', $producto)}}"><button class="btn btn-primary mb-3" type="submit">Agregar</button></a></td>
                {{--<td><a href="{{route('productos.create', $producto)}}">Agregar Producto</a></td>--}}
                
            </tr>
             
                            
            @endforeach
            
        </tbody>
            
    </table>

</form>
@endsection


