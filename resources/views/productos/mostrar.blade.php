@extends('layouts.plantilla')

@section('title','Producto', $producto->nombre)

@section('content')
<br>
<h2 class="display-4 text-center my -5">El producto seleccionado es:</h2>
    <br>
    
    <div class="row justify-content-center">

      <div class="card" style="width: 18rem;">
        
        <div class="row justify-content-center">
          <div class="col-md-9"> 
                <img src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" class="img-fluid rounded-start" alt="card image cap">
            </div>
           </div>
          
      </div>
      <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card-body">
          <h4 class="card-title">{{$producto->nombre}}</h4>
          <br>
          <h5><p  class="card-text">Descripción: {{$producto->descripcion}}</p></h5>
          <h5><p class="card-text">Precio ($): {{$producto->precio}}</p></h5>
          <h5><p class="card-text">Cantidad: {{$producto->cantidad}}</p></h5>
          <h5><p class="card-text">Fecha de Vencimiento: {{$producto->fecha_vencimiento}}</p></h5>
          <h5><p class="card-text">Tipo: {{$producto->tipo}}</p></h5>
          <h5><p class="card-text">IVA (%): {{$producto->iva}}</p></h5>
          <br>
          <td><a href="{{route('productos.mostrarComprar', $producto)}}"><button class="btn btn-outline-primary btn-space" type="submit">Comprar Producto</button></a></td>
          <form action="{{route('productos.show', $producto)}}" method="GET">
            <br>
            @method('delete')
            {{-- <div class="col-md-1"> --}}
                  {{-- <button class="btn btn-outline-primary btn-space" type="submit">Eliminar Producto</button> --}}
            {{-- </div> --}}
            
            @csrf
      
    </div>
        </form>
        </div>
      </div>
      </div>
    </div>
    
    </div>
    <br>
      </tbody> 
    
   {{--<h1>Eliminar productos</h1>--}}
    {{--<p><strong><h2>El producto a eliminar es: {{$producto->nombre}}</h2></strong></p>--}}
    {{--<p><strong><h3>Nombre: </strong>{{$producto->nombre}}</h3></p>
    <p><strong><h3>Descripcion: </strong>{{$producto->descripcion}}</h3></p>
    <p><strong><h3>Cantidad: </strong>{{$producto->precio}}</h3></p>
    <p><strong><h3>Precio: </strong>{{$producto->cantidad}}</h3></p>
    <p><strong><h3>Fecha de Vencimiento: </strong>{{$producto->fecha_vencimiento}}</h3></p>
    <p><strong><h3>Tipo: </strong>{{$producto->tipo}}</h3></p>
    <p><strong><h3>IVA: </strong>{{$producto->iva}}</h3></p>

        
    <div>
        <iframe weight = "300px" height="300px" src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" frameborder="0"></iframe>
    </div> 
--}}
{{-- <div class="container">
<div class="row justify-content-center"> --}}
    {{-- <div class="col-md-2">
  <a href="{{route('productos.edit', $producto)}}"><button class="btn btn-outline-primary btn-space" type="submit">Editar Productos</button ></a> 
    </div> 
    <div class="col-mb-3">
  <td><a href="{{route('productos.create')}}"><button class="btn btn-outline-primary btn-space" type="submit">Registrar un nuevo producto</button></a></td>
    </div> --}}
  {{-- <div class="col-md-2">
    
  </div> --}}
 {{-- <form action="{{route('productos.show', $producto)}}" method="GET">
        @csrf
        @method('delete')
  <div class="col-md-2">
        <button class="btn btn-outline-primary btn-space" type="submit">Eliminar Producto</button>
  </div>
</div>
    </form> --}}
</div>
</div>

  

@endsection