@extends('layouts.plantilla')

@section('title','Producto', $producto->nombre)

@section('content')
<br>
  <h2 class="display-4 text-center my -5"> El producto a eliminar es:</h2>
  <br>

    <div class="row justify-content-center">

      <div class="card" style="width: 18rem;">
        
        <div class="row g-0">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <img src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" class="img-fluid rounded-start" alt="card image cap">
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">{{$producto->nombre}}</h5>
                <p class="card-text">Descripción: {{$producto->descripcion}}</p>
                <p class="card-text">Precio ($): {{$producto->precio}}</p>
                <p class="card-text">Cantidad: {{$producto->cantidad}}</p>
                <p class="card-text">Fecha de Vencimiento: {{$producto->fecha_vencimiento}}</p>
                <p class="card-text">Tipo: {{$producto->tipo}}</p>
                <p class="card-text">IVA (%): {{$producto->iva}}</p>

                
                
              </div>
              <a href="{{route('productos.edit', $producto)}}"><button class="btn btn-primary mb-3" type="submit">Editar Producto</button></a>
    
                <td><a href="{{route('productos.create')}}"><button class="btn btn-primary mb-3" type="submit">Registrar Nuevo Producto</button></a></td>
    
                <form action="{{route('productos.destroy', $producto)}}" method="POST">
                  @csrf
                  @method('delete')
                  
                  <button class="btn btn-primary mb-3" type="submit">Eliminar Producto</button>
                </form>
              </div>      
          </div>    
        </div>
      </div> 
    </div>

    
  
    
   {{--<h1>Eliminar productos</h1>--}}
    {{--<p><strong><h2>El producto a eliminar es: {{$producto->nombre}}</h2></strong></p>--}}
    {{-- <p><strong><h3>Nombre: </strong>{{$producto->nombre}}</h3></p>
    <p><strong><h3>Descripcion: </strong>{{$producto->descripcion}}</h3></p>
    <p><strong><h3>Cantidad: </strong>{{$producto->precio}}</h3></p>
    <p><strong><h3>Precio: </strong>{{$producto->cantidad}}</h3></p>
    <p><strong><h3>Fecha de Vencimiento: </strong>{{$producto->fecha_vencimiento}}</h3></p>
    <p><strong><h3>Tipo: </strong>{{$producto->tipo}}</h3></p>
    <p><strong><h3>IVA: </strong>{{$producto->iva}}</h3></p> --}}

        
    

    
   {{--<a href="{{route('productos.edit', $producto)}}"><button type="submit"><h3>Eliminar Producto</h3></button></a>
       --}} 

      


       {{-- <div class="container py-3">
        <div class="title h1 text-center">Horizontal cards - Bootstrap 4</div>
        <!-- Card Start -->
        <div class="card">
          <div class="row ">
            <div class="col-md-7 px-3">
              
              <div class="card-block px-4">
                <h5 class="card-title">Nombre </h5>
                <p class="card-text">{{$producto->nombre}}</p>
                <h5 class="card-title">Descripcion </h5>
                <p class="card-text">{{$producto->descripcion}}</p>
                <h5 class="card-title">Precio </h5>
                <p class="card-text">{{$producto->precio}}</p>
                <h5 class="card-title">Cantidad </h5>
                <p class="card-text">{{$producto->cantidad}}</p>
                <h5 class="card-title">Fecha de Vencimiento </h5>
                <p class="card-text">{{$producto->fecha_vencimiento}}</p>
                <h5 class="card-title">Tipo </h5>
                <p class="card-text">{{$producto->tipo}}</p>
                <h5 class="card-title">Iva </h5>
                <p class="card-text">{{$producto->iva}}</p>

              </div>
         --}}
                
                
              
                
                {{-- <a href="{{route('productos.edit', $producto)}}"><button class="btn btn-primary mb-3" type="submit">Editar Producto</button></a>
    
                <td><a href="{{route('productos.create')}}"><button class="btn btn-primary mb-3" type="submit">Registrar Nuevo Producto</button></a></td>
    
                <form action="{{route('productos.destroy', $producto)}}" method="POST">
                  @csrf
                  @method('delete')
                  
                  <button class="btn btn-primary mb-3" type="submit">Eliminar Producto</button>
                </form>
              
            </div>
            <!-- Carousel start -->
            <div class="col-md-5">
              <div id="CarouselTest" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#CarouselTest" data-slide-to="0" class="active"></li>
                  {{-- <li data-target="#CarouselTest" data-slide-to="1"></li>
                  <li data-target="#CarouselTest" data-slide-to="2"></li> --}}
      
                {{-- </ol>
                <div class="carousel-inner"> --}}
                  {{-- <div class="carousel-item active">
                    <img class="d-block" src="https://picsum.photos/450/300?image=1072" alt="">
                  </div> --}}
                  {{-- <div class="carousel-item active col-md-6">
                    <img src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" class="img-fluid rounded-start" alt="card image cap">
                  </div> --}}
                  {{-- <div class="carousel-item">
                    <img class="d-block" src="https://picsum.photos/450/300?image=855" alt="">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block" src="https://picsum.photos/450/300?image=355" alt="">
                  </div> --}}
                  {{-- <a class="carousel-control-prev" href="#CarouselTest" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
                  <a class="carousel-control-next" href="#CarouselTest" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> --}}
                {{-- </div>
              </div>
            </div>
            <!-- End of carousel -->
          </div>
        </div>
        <!-- End of card -->
      
      </div> --}} 
      
      
@endsection