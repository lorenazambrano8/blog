@extends('layouts.plantilla')

@section('title','producto')

@section('content')
    <div class="container">
        <br>
        <h2 class="display-4 text-center my -5"> Productos</h2>
        <h2 class="display-7 text-lef my-3">Registre el producto:</h2>

        <form action="{{route('productos.store')}}" method="POST" enctype="multipart/form-data">

        @csrf
        <div class="form-row">

        <div class="form-group col-md-6">
            <label>Nombre</label>
            <input name="nombre" type="text" class="form-control" placeholder="Ejemplo: Jugos" value="{{old('nombre')}}">
        @error('nombre')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>
        
        <div class="form-group col-md-6">
            <label>Descripción</label>
            <input name="descripcion" type="text" class="form-control" placeholder="Ejemplo: " value="{{old('descripcion')}}">
        @error('descripcion')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>
    
        <div class="form-group col-md-6">
            <label>Precio ($)</label>
            <input name="precio" type="text" class="form-control" placeholder="Ejemplo: 1200,5000, 10000, 100000 " value="{{old('precio')}}">
            @error('precio')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>
    
        <div class="form-group col-md-6">
            <label>Cantidad</label>
            <input name="cantidad" type="text" class="form-control"  placeholder="Ejemplo: 10,100,20, etc" value="{{old('cantidad')}}">
         @error('cantidad')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>

        <div class="form-group col-md-6">
            <label>Fecha de Vencimiento</label>
            <input name="fecha_vencimiento" type="date" class="form-control"  placeholder="Ejemplo: 2022/10/04 " value="{{old('fecha_vencimiento')}}">
        @error('fecha_vencimiento')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>

        <div class="form-group col-md-6">
            <label>Tipo</label>
            <input name="tipo" type="text" class="form-control"  placeholder="Ejemplo: Bebidas, Lacteos, Harinas, etc." value="{{old('tipo')}}">
        @error('tipo')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>

        <div class="form-group col-md-6">
            <label>IVA (%)</label>
            <input name="iva" type="text" class="form-control" placeholder="Ejemplo: 19" value="{{old('iva')}}">
        @error('iva')
            <small class="text-danger">{{$message}}</small>                
        @enderror
        </div>
        
        <div class="form-group col-md-6">
            <label>Adjuntar Archivo Imagen</label>
            <input type="file" name="urlPdf" class="form-control-file" accept="image/*">
            @error('urlPdf')
                <small class="text-danger">{{$message}}</small>                
            @enderror
        </div>
    
        <button class="btn btn-primary mb-3" type="submit">Enviar Formulario</button>
        </form>
    </div> 
@endsection

