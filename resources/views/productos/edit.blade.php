@extends('layouts.plantilla')

@section('title','Prodcutos Edit')

@section('content')

<div class="container">
    <br>    
    <h2 class="display-4 text-center my -5">Editar un producto:</h2>
    
    <br>
    <form action="{{route('productos.update', $producto)}} "method="POST" enctype="multipart/form-data">
 
    @csrf
    @method('put')

    <div class="form-row">

    <div class="form-group col-md-6">
        <label>Nombre</label>
        <input name="nombre" type="text" class="form-control" value="{{old('nombre', $producto->nombre)}}">
    </div>

    <div class="form-group col-md-6">
        <label>Descripción</label>
        <input name="descripcion" type="text" class="form-control" value="{{old('descripcion', $producto->descripcion)}}">
    </div>

    <div class="form-group col-md-6">
        <label>Precio ($)</label>
        <input name="precio" type="text" class="form-control" value="{{old('precio', $producto->precio)}}">
    </div>

    <div class="form-group col-md-6">
        <label>Cantidad</label>
        <input name="cantidad" type="text" class="form-control"  value="{{old('cantidad', $producto->cantidad)}}">
    </div>

    <div class="form-group col-md-6">
        <label>Fecha de Vencimiento</label>
        <input name="fecha_vencimiento" type="date" class="form-control" value="{{old('fecha_vencimiento', $producto->fecha_vencimiento)}}">
    </div>

    <div class="form-group col-md-6">
        <label>Tipo</label>
        <input name="tipo" type="text" class="form-control" value="{{old('tipo', $producto->tipo)}}">
    </div>

    <div class="form-group col-md-6">
        <label>IVA (%)</label>
        <input name="iva" type="text" class="form-control" value="{{old('iva', $producto->iva)}}">
    </div>
    

    <div class="form-group col-md-6">
        <label>Adjuntar Archivo Imagen</label>
        <input type="file" name="urlPdf" class="form-control-file" accept="image/*">
        @error('urlPdf')
            <small class="text-danger">{{$message}}</small>                
        @enderror
    </div>
     
    

    <button class="btn btn-primary mb-3" type="submit">Enviar Formulario</button>
    </form>
</div> 

@endsection