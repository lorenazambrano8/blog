@extends('layouts.plantilla')

@section('title','Productos')

@section('content')
<head>    
    {{-- <h2 class="display-4 text-center my -5">Bienvenido a la página principal</h2> --}}
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
     --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <style>.carousel-inner > .item > img { width:100%; height:570px;}
        
    </style>  
    
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        
        <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="http://localhost/blog/images/cric-slider-2.png" alt="buffalo-skyline"  width="640" height="300">
                </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="http://localhost/blog/images/carousel-slider-1.png" alt="buffalo-skyline"  width="640" height="300">
                    </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="http://localhost/blog/images/jugos-slider-1.png" alt="buffalo-skyline"  width="640" height="300">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="http://localhost/blog/images/baner.png" alt="buffalo-skyline"  width="640" height="300">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="http://localhost/blog/images/baner2.png" alt="buffalo-skyline"  width="640" height="300">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="http://localhost/blog/images/cerveza-slider.png" alt="buffalo-skyline"  width="640" height="300">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="http://localhost/blog/images/baner1.png" alt="buffalo-skyline"  width="640" height="300">
                        </div>
                        
                    {{-- <ol class="carousel-indicators">
                        <li data-target="#carouselExamplesSlidesOnly" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExamplesSlidesOnly" data-slide-to="1" class="active"></li>
                        <li data-target="#carouselExamplesSlidesOnly" data-slide-to="2" class="active"></li>
                    </ol> --}}
        
                </div>
            </div>
    <br>
    <table class="table table-bordered table-striped texter-center table-hover">
    
        <tbody>
            <div class="container">
                

                <div class="card-deck">
                    <div class="row">
                        @foreach ($producto as $producto)

                            <div class="col-md-2 mb-2"> 
                                <div class="card">
                                    <div class="row justify-content-center">
                                        <div class="card" style="width: 18rem;">
                                            
                                        
                                            <img  src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" class="card-img-top" alt="Card image cap">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$producto->nombre}}</h4>
                                                <h5 class="card-title">{{$producto->precio}}</h5>
                                                <h5 class="card-title">{{$producto->descripcion}}</h5>

                                                <div class="row justify-content-center">

                                                  <a href="{{route('productos.mostrar',$producto)}}"><button class="btn btn-primary" type="submit">Mas Información</button></a> 
                                                
                                                </div>

                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
              
            </div>

        </tbody>
    </table>

</form>
</head>  
<body>
    <div class="jumbotron text-center" style="margin-bottom:0">
        <div class="row">
            <div class="col-md-12">
              <a href="https://web.facebook.com/programaeconomicoambiental" target="_blank" class="btn-social btn-facebook"><i class="fa fa-facebook"></i></a>
              {{-- <a href="http://scripteden.com/download/eden-ui/" target="_blank" class="btn-social btn-google-plus"><i class="fa fa-google-plus"></i></a> --}}
              <a href="https://www.instagram.com" target="_blank" class="btn-social btn-instagram"><i class="fa fa-instagram"></i></a>
              <a href="http://scripteden.com/download/eden-ui/" target="_blank" class="btn-social btn-twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.youtube.com/" target="_blank" class="btn-social btn-youtube"><i class="fa fa-youtube"></i></a>
              <a href="http://scripteden.com/download/eden-ui/" target="_blank" class="btn-social btn-email"><i class="fa fa-whatsapp"></i></a></a>
             </div>
        </div>
        <div class="col-md-14">
            <h5>&copy;MercadoMinga 2021</h5>
					<h5>Desarrollado por MercadoMinga.com</h5>
</body>
@endsection


{{-- <form action="{{route('productos.create')}}">
    <br>
    <h2 class="display-4 text-center my -5"> Lista de Productos</h2>
    <br>
    <table class="table table-bordered table-striped texter-center table-hover">
    
        <tbody>
            <div class="container">

                <div class="card-deck">
                    <div class="row">
                        @foreach ($producto as $producto)

                            <div class="col-md-4 mb-2"> 
                                <div class="card">
                                    
                                    <div class="card" style="width: 18rem;">
                                        <img  src="{{'http://localhost/blog/public/storage/imagenes/'.$producto->urlPdf}}" class="card-img-top" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$producto->nombre}}</h5>
                                            <h5 class="card-title">{{$producto->precio}}</h5>
                                            <h5 class="card-title">{{$producto->descripcion}}</h5>

                                            <a href="{{route('productos.mostrar',$producto)}}" class="btn btn-primary">Mas Información</a> 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </tbody>
    </table>
</form>  --}}
