@extends('layouts.plantilla')

@section('title','Cliente Edit')

@section('content')

<div class="container">
<br>
<br>
<br>
<h2 class="display-7 text-lef my-3">Editar Cliente:</h2>
<br>

<form action="{{route('clientes.update', $cliente)}} "method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-row">

            <div class="form-group col-md-6">
                <label>Nombre y Apellido</label>
                <input name="nombre" type="text" class="form-control" value="{{old('nombre', $cliente->nombre)}}">
            </div>
        
            <div class="form-group col-md-6">
                <label>Identificación</label>
                <input name="cedula_ciudadania" type="text" class="form-control" value="{{old('cedula_ciudadania', $cliente->cedula_ciudadania)}}">
            </div>

            <div class="form-group col-md-6">
                <label>Dirección</label>
                <input name="direccion" type="text" class="form-control" value="{{old('direccion', $cliente->direccion)}}">
            </div>
            
            <div class="form-group col-md-6">
                <label>Telefono</label>
                <input name="telefono" type="text" class="form-control" value="{{old('telefono', $cliente->telefono)}}">
            </div>

            <div class="form-group col-md-6">
                <label>E-mail</label>
                <input name="correo" type="text" class="form-control" value="{{old('correo', $cliente->correo)}}">
            </div>

            <div class="form-group col-md-6">
                <label></label>
            </div>
 
            <div class="display-7 text-lef my-3">
                <button class="btn btn-primary mb-5" type="submit">Enviar Formulario</button>
             </div>
        </div>
        </form>
    
</div>

@endsection