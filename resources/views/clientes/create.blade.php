@extends('layouts.plantilla')

@section('title','Cliente')

@section('content')
<div class="container">
    <br>
    <h2 class="display-4 text-center my -5">Bienvenido a Clientes</h2>
    <h2 class="display-7 text-lef my-3">Ingrese datos del cliente:</h2>

    <form action="{{route('clientes.store')}}" method="POST" enctype="multipart/form-data">

        @csrf

        <div class="form-row">

            <div class="form-group col-md-6">
                <label>Nombre y Apellido</label>
                <input name="nombre" type="text" class="form-control" placeholder="Ejemplo: Juan Velez" value="{{old('nombre')}}">
            @error('nombre')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Identificacion</label>
                <input name="cedula_ciudadania" type="text" class="form-control" placeholder="Ejemplo: 1062007008" value="{{old('cedula_ciudadania')}}">
            @error('cedula_ciudadania')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>
        
            <div class="form-group col-md-6">
                <label>Dirección</label>
                <input name="direccion" type="text" class="form-control" placeholder="Ejemplo: calle 2 # 51-29" value="{{old('direccion')}}">
            @error('direccion')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Teléfono</label>
                <input name="telefono" type="text" class="form-control" placeholder="Ejemplo: 3118672341" value="{{old('telefono')}}">
            @error('telefono')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>E-mail</label>
                <input name="correo" type="text" class="form-control" placeholder="Ejemplo: example@gmail.com" value="{{old('correo')}}">
            @error('correo')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label></label>
            </div>

        <button class="btn btn-primary mb-4" type="submit">Enviar Formulario</button>
    </form>
</div>
@endsection