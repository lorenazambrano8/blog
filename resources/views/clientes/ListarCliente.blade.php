@extends('layouts.plantilla')

@section('title','Cliente')



@section('content')
<h2 class="display-4 text-center my-4">Clientes Agregados</h2>
    <table class="table table-success table-striped table-hover">
        
        <thead>
            <tr>
                <th class="table-dark"><h3>Id</h3></th>
                <th class="table-dark"><h3>Nombre</h3></th>
                <th class="table-dark"><h3>Identificacion</h3></th>
                <th class="table-dark"><h3>Direccion</h3></th>
                <th class="table-dark"><h3>Telefono</h3></th>
                <th class="table-dark"><h3>Correo</h3></th>
                <th class="table-dark"><h3></h3></th>
                <th class="table-dark"><h3>Acciones</h3></th>
                <th class="table-dark"><h3></h3></th>
            </tr>
         </thead>
         <tbody>
             @foreach ($clientes as $cliente)
             <tr>
          
                 <td>{{$cliente->id}}</td>
                 <td>{{$cliente->nombre}}</td>
                 <td>{{$cliente->cedula_ciudadania}}</td>
                 <td>{{$cliente->direccion}}</td>
                 <td>{{$cliente->telefono}}</td>
                 <td>{{$cliente->correo}}</td>
                 
                 
                 <td><a href="{{route('clientes.show',$cliente)}}"><button class="btn btn-primary mb-3" type="submit">Eliminar Cliente</button>
                 </a></td>
                 <td><a href="{{route('clientes.edit',$cliente)}}"><button class="btn btn-primary mb-3" type="submit">Editar Cliente</button>
                 </a></td>
                 <td><a href="{{route('clientes.create', $cliente)}}"><button class="btn btn-primary mb-3" type="submit">Agregar Cliente</button>
                 </a></td>
             </tr>
             
                            
            @endforeach
            
        </tbody>
            
    </table>


@endsection
