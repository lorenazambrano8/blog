@extends('layouts.plantilla')

@section('title','Clientes', $cliente->nombre)

@section('content')

<h2 class="display-4 text-center my-4">El cliente a eliminar es:</h2>

<table class="table table-success table-striped table-hover">
    {{-- <div class="row justify-content-center"> --}}
    <thead>
        <tr>
            <th class="table-dark"><h3>Nombre:</h3> 
            <th class="table-dark"><h3>Identificacion:</h3> 
            <th class="table-dark"><h3>Direccion:</h3> 
            <th class="table-dark"><h3>Telefono:</h3>
            <th class="table-dark"><h3>Correo:</h3>                      
        </th>
    </thead>
    <body>     
        <td> {{$cliente->nombre}}</td>
        <td> {{$cliente->cedula_ciudadania}} </td>
        <td> {{$cliente->direccion}} </td>
        <td> {{$cliente->telefono}} </td>
        <td> {{$cliente->correo}} </td>
        </tr>
      </body>
    
</table>
<div class="row justify-content-center"> 

<a class="btn" href="{{route('clientes.edit', $cliente)}}"><button class="btn btn-outline-dark btn-space" type="submit">Editar Cliente</button></a>

<a class="btn " href="{{route('clientes.create')}}"><button class="btn btn-outline-dark btn-space" type="submit">Registrar Cliente Nuevo</button></a>  
</div>

<div class="row justify-content-center">
<form action="{{route("clientes.destroy", $cliente)}}" method="POST">
    @csrf
    @method('delete')
    <br>
    <a class="btn btn-ligth" href=""><button class="btn btn-outline-dark btn-space" type="submit">Eliminar Cliente</button></a>
</form>
</div>

@endsection