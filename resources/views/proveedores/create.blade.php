@extends('layouts.plantilla')

@section('title','Proveedor')

@section('content')
    <div class="container">
    <br>
    <br>
    <h2 class="display-4 text-center my -5"> Proveedores</h2>
    <br>
    <h2>Ingrese los datos del proveedor:</h2>
    <br>

    <form action="{{route('proveedores.store')}}" method="POST" enctype="multipart/form-data">

        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nombre y Apellido</label>
                <input name="nombre" type="text" class="form-control" placeholder="Ejemplo: Juan Perez" value="{{old('nombre')}}">
            @error('nombre')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>
            
            <div class="form-group col-md-6">
                <label>NIT</label>
                <input name="nit" type="text" class="form-control" placeholder="Ejemplo: 1062345987" value="{{old('nit')}}">
            @error('nit')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Dirección</label>
                <input name="direccion" type="text" class="form-control" placeholder="Ejemplo: Calle 5a # 34-87, Popayán-Cauca" value="{{old('direccion')}}">
            @error('direccion')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Teléfono</label>
                <input name="telefono" type="text" class="form-control" placeholder="Ejemplo: 3118762348" value="{{old('telefono')}}">
            @error('telefono')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label>E-mail</label>
                <input name="correo" type="text" class="form-control" placeholder="Ejemplo: example@gmail.com" value="{{old('correo')}}">
            @error('correo')
                <small class="text-danger">{{$message}}</small>                
            @enderror
            </div>

            <div class="form-group col-md-6">
                <label></label>
            </div>

            <button class="btn btn-primary mb-3" type="submit">Enviar Formulario</button>
    </form>
@endsection