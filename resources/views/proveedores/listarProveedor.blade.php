@extends('layouts.plantilla')

@section('title','Proveedores')


@section('content')

<h2 class="display-4 text-center my-5">Lista de Proveedores</h2>

    <table class="table table-success table-striped table-hover">
        <thead>
            <tr>
                <th class="table-dark"><h3>Id</h3></th>
                <th class="table-dark"><h3>Nombre</h3></th>
                <th class="table-dark"><h3>NIT</h3></th>
                <th class="table-dark"><h3>Direccion</h3></th>
                <th class="table-dark"><h3>Telefono</h3></th>
                <th class="table-dark"><h3>Correo</h3></th>
                <th class="table-dark"><h3></h3></th>
                <th class="table-dark"><h3>Acciones</h3></th>
                <th class="table-dark"><h3></h3></th>
                
         </thead>
         <tbody>
             @foreach ($proveedores as $proveedor)
             <tr>
                 <td>{{$proveedor->id}}</td>
                 <td>{{$proveedor->nombre}}</td>
                 <td>{{$proveedor->nit}}</td>
                 <td>{{$proveedor->direccion}}</td>
                 <td>{{$proveedor->telefono}}</td>
                 <td>{{$proveedor->correo}}</td>
                                 
                 <td><a href="{{route('proveedores.show',$proveedor)}}"><button class="btn btn-primary mb-3" type="submit">Eliminar Proveedor</button></a></td>
                 <td><a href="{{route('proveedores.edit',$proveedor)}}"><button class="btn btn-primary mb-3" type="submit">Editar Proveedor</button></a></td>
                 <td><a href="{{route('proveedores.create', $proveedor)}}"><button class="btn btn-primary mb-3" type="submit">Agregar Proveedor</button></a></td>  
                 
                </tr>
            @endforeach
            
    </table>
   
@endsection
