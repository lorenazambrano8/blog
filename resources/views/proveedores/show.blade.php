@extends('layouts.plantilla')

@section('title','Proveedor', $proveedor->nombre)

@section('content')
<h2 class="display-4 text-center my-4">Eliminar Proveedor</h2>

    <table class="table table-success table-striped table-hover">
        <thead>      
            <tr>
                <th class="table-dark"><h3>Nombre:</h3></th>
                <th class="table-dark"><h3>NIT:</h3></th>
                <th class="table-dark"><h3>Direccion:</h3></th>
                <th class="table-dark"><h3>Telefono:</h3></th>
                <th class="table-dark"><h3>Correo:</h3></th>
            </tr>
        </thead>
        <tbody>
             <tr>
                <td>{{$proveedor->nombre}}</td>
                <td>{{$proveedor->nit}}</td>
                <td>{{$proveedor->direccion}}</td>
                <td>{{$proveedor->telefono}}</td>
                <td>{{$proveedor->correo}}</td>               
             </tr>       
        </body>      
    </table>

    <div class="row justify-content-center">          
        <a  class="btn" href="{{route('proveedores.edit', $proveedor)}}"><button class="btn btn-outline-dark btn-space" type="submit">Editar Proveedor</button></a>
        <a  class="btn" href="{{route('proveedores.create')}}"><button class="btn btn-outline-dark btn-space" type="submit">Registrar Nuevo Proveedor</button></a>  
    </div>
    <div class="row justify-content-center">            
        <form action="{{route("proveedores.destroy", $proveedor)}}" method="POST">
            @csrf
            @method('delete')       
            <a class="btn btn-ligth" href=""><button class="btn btn-outline-dark btn-space" type="submit">Eliminar Proveedor</button></a>
        </form>
    </div>  
@endsection