@extends('layouts.plantilla')

@section('title','Proveedor Edit')

@section('content')
<div class="container">
    <br>
    <br>
    <br>
    <h2 class="display-7 text-lef my-3">Editar Proveedor:</h2>
    <br>

    <form action="{{route("proveedores.update",$proveedor)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-row">

            <div class="form-group col-md-6">
                <label>Nombre y Apellido</label>
                <input name="nombre" type="text" class="form-control" value="{{old('nombre', $proveedor->nombre)}}">
            </div>

            <div class="form-group col-md-6">
                <label>NIT</label>
                <input name="nit" type="double" class="form-control" value="{{old('nit', $proveedor->nit)}}">
            </div>

            <div class="form-group col-md-6">
                <label>Dirección</label>
                <input name="direccion" type="text" class="form-control" value="{{old('direccion', $proveedor->direccion)}}">
            </div>

            <div class="form-group col-md-6">
                <label>Teléfono</label>
                <input name="telefono" type="integer" class="form-control" value="{{old('telefono', $proveedor->telefono)}}">
            </div>

            <div class="form-group col-md-6">
                <label>E-mail</label>
                <input name="correo" type="text" class="form-control" value="{{old('correo', $proveedor->correo)}}">
            </div>

            <div class="form-group col-md-6">
                <label></label>
            </div>

        <div class="display-7 text-lef my-3">
                <button class="btn btn-primary mb-5" type="submit">Actualizar Datos</button>
             </div>
        </div>
    </form>

@endsection