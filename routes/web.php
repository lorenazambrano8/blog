<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProveedorController;
use App\Models\Producto;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class)->name('home');
Route::get('cursos', [CursoController::class, 'index'])->name('cursos.index');
Route::get('cursos/create', [CursoController::class, 'create'])->name('cursos.create'); 
Route::post('cursos', [CursoController::class, 'store'])->name('cursos.store'); 
Route::get('cursos/{curso}', [CursoController::class, 'show'])->name('cursos.show');

//crear productos

Route::get('productos', [ProductoController::class, 'index'])->name('productos.index');
Route::get('productos/create', [ProductoController::class, 'create'])->name('productos.create'); 
Route::post('producto', [ProductoController::class, 'store'])->name('productos.store');
Route::get('productos/{producto}', [ProductoController::class, 'show'])->name('productos.show');
Route::get('productos/{producto}/edit', [ProductoController::class, 'edit'])->name('productos.edit');
Route::put('productos/{producto}', [ProductoController::class, 'update'])->name('productos.update');
Route::delete('productos/{producto}', [ProductoController::class, 'destroy'])->name('productos.destroy');
Route::get('productos/{producto}/comprar', [ProductoController::class, 'comprar'])->name('productos.comprar');
Route::get('productos/{producto}/mostar', [ProductoController::class, 'mostrar'])->name('productos.mostrar');
Route::get('productos/{producto}/mostarComprar', [ProductoController::class, 'mostrarComprar'])->name('productos.mostrarComprar');
Route::get('productos/{producto}/{cliente}/factura', [ProductoController::class, 'factura'])->name('productos.factura');
// route::get('productos', [ProductoController::class, 'principal'])->name('productos.principal');
//crear clientes

Route::get('clientes', [ClienteController::class, 'index'])->name('clientes.index');
Route::get('clientes/create', [ClienteController::class, 'create'])->name('clientes.create'); 
Route::post('cliente', [ClienteController::class, 'store'])->name('clientes.store');
Route::get('clientes/{cliente}', [ClienteController::class, 'show'])->name('clientes.show');
Route::get('clientes/{cliente}/edit', [ClienteController::class, 'edit'])->name('clientes.edit');
Route::put('clientes/{cliente}', [ClienteController::class, 'update'])->name('clientes.update');
Route::delete('clientes/{cliente}', [ClienteController::class, 'destroy'])->name('clientes.destroy');

//crear proveedores

Route::get('proveedores', [ProveedorController::class, 'index'])->name('proveedores.index');
Route::get('proveedores/create', [ProveedorController::class, 'create'])->name('proveedores.create'); 
Route::post('proveedor', [ProveedorController::class, 'store'])->name('proveedores.store');
Route::get('proveedores/{proveedor}', [ProveedorController::class, 'show'])->name('proveedores.show');
Route::get('proveedores/{proveedor}/edit', [ProveedorController::class, 'edit'])->name('proveedores.edit');
Route::put('proveedores/{proveedor}', [ProveedorController::class, 'update'])->name('proveedores.update');
Route::delete('proveedores/{proveedor}', [ProveedorController::class, 'destroy'])->name('proveedores.destroy');




/*Route::get('cursos/{curso}/{categoria?}', function ($curso, $categoria = null) {
    if($categoria){
        return "Bienvenido al curso: $curso, de la categoria: $categoria";        
    }else{
        Return "bienvenido al curso: $curso";
    }
      
});*/

