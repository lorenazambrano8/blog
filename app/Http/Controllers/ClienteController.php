<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(){
        $clientes = Cliente::all();
        return view('clientes.listarCliente',compact('clientes'));
    }
    
    public function create(){
        return view('clientes.create');
    }

    public function store(Request $request){
        //return $request->all();
        $request->validate([
            'nombre'=>'required',
            'cedula_ciudadania'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'correo' =>'required'
        ]);
        $cliente = new Cliente();

        $cliente->nombre = $request->nombre;
        $cliente->cedula_ciudadania = $request->cedula_ciudadania;
        $cliente->direccion = $request->direccion;
        $cliente->telefono = $request->telefono;
        $cliente->correo = $request->correo;
        
        $cliente->save();
        $clientes = Cliente::all();
        return view('clientes.listarCliente',compact('clientes'));
        //return '<h2>Cliente Registrado con éxito...</h2>';

    }
    public function show(Cliente $cliente){
        return view('clientes.show',compact('cliente'));
    }

    public function edit(Cliente $cliente){
        //$producto = Producto::find($producto);
        return view('clientes.edit', compact('cliente'));

    }

    public function update(Request $request, Cliente $cliente){
        $cliente ->nombre = $request->nombre;
        $cliente ->cedula_ciudadania = $request->cedula_ciudadania;
        $cliente ->direccion = $request->direccion;
        $cliente ->telefono = $request->telefono;
        $cliente ->correo = $request->correo;
            
        
        $cliente->save();

        return redirect()->route('clientes.index', $cliente);   
    }
    public function destroy(Cliente $cliente){
        $cliente->delete();

        return redirect()->route('clientes.index');
    }
}
