<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Cliente;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;

class ProductoController extends Controller
{
    public function index(){
        $producto = Producto::all();
        return view('productos.listarProducto',compact('producto'));
        
    }

    // public function principal(){
    //     $producto = Producto::all();
    //     return view('productos.listar',compact('producto'));
        
    // }

    public function mostrarProductos(){
        $producto = Producto::all();
        return view('productos.listarProductoComprar',compact('producto'));
    }

    public function create(){
        return view('productos.create');
    }

    public function store(Request $request){

        $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required',
            'precio'=>'required',
            'cantidad'=>'required',
            'fecha_vencimiento'=>'required',
            'tipo'=>'required',
            'iva'=>'required',
            'urlPdf' =>'required'
        ]);
        
        $producto = new Producto();

        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->cantidad = $request->cantidad;
        $producto->fecha_vencimiento = $request->fecha_vencimiento;
        $producto->tipo = $request->tipo;
        $producto->iva = $request->iva;

        //$producto->save();
        
        
        //return($productos);
        
        //return '<h2>Producto Registrado con éxito...</h2>';

        //adjuntar imagen 
        $file=$request->file("urlPdf");
        $nombreArchivo = "img_".time().".".$file->guessExtension();
        $request->file('urlPdf')->storeAs('public/imagenes',$nombreArchivo);
        $producto->urlPdf=$nombreArchivo;
        $producto->save();

        $producto = Producto::all();
        return view('productos.listarProducto',compact('producto'));

    }

    public function show(Producto $producto){
        return view('productos.show',compact('producto'));
    }

    public function mostrar(Producto $producto){
        return view('productos.mostrar', compact('producto'));
    }

    public function edit(Producto $producto){
        //$producto = Producto::find($producto);
        return view('productos.edit', compact('producto'));

    }

    public function update(Request $request, Producto $producto){
        
        $producto ->nombre = $request->nombre;
        $producto ->descripcion = $request->descripcion;
        $producto ->precio = $request->precio;
        $producto ->cantidad = $request->cantidad;
        $producto ->fecha_vencimiento = $request->fecha_vencimiento;
        $producto ->tipo = $request->tipo;
        $producto->iva = $request->iva;
           
        // $file=$request->file("urlPdf");
        // $nombreArchivo = "img_".time().".".$file->guessExtension();
        // $request->file('urlPdf')->storeAs('public/imagenes',$nombreArchivo);
        // $producto->urlPdf=$nombreArchivo;
        
        $producto->save();

        //$producto = Producto::all();
        //return view('productos.listarProducto',compact('producto'));

        //$producto->save();

        return redirect()->route('productos.index');
    }

    public function destroy(Producto $producto){
        $producto->delete();

        return redirect()->route('productos.index');
    }

    public function mostrarComprar(Producto $producto){
        
       return view('productos.mostrarComprar', compact('producto'));

    }

    public function comprar(Producto $producto, Cliente $cliente){

        $cliente = Cliente::find(9);


        if ( $producto->cantidad >0){
            $producto->cantidad-=1;
            $producto->save();

            // return $producto->cantidad;
        }else{
            return 'Producto Agotado';
        }
            
        $cliente->producto()->attach($producto);
        
        return view('productos.factura', compact('producto','cliente'));
        //return $producto;       
            // return $producto;
          
          // $cliente =  Cliente::find(1);
          // $cliente->producto()->attach($producto);
          // return view('productos.mostrarcompra', compact('producto'));
          // // return redirect()->route('productos.list');
          /*if ($producto->cantidad > 0) {
            $cliente =  Cliente::find(2);
            $producto->cantidad-=1;
            $producto->save();
          }
          else{
            
            return 'Agotado'. redirect()->route('productos.index');
          }*/
          
          
          //  return $producto->cantidad;
          
          // $cliente->producto()->attach($producto);
           //return redirect()->route('productos.index', $producto);
        //    return view('productos.mostrarComprar', compact('producto'))
    }
    
    public function admin(Producto $producto){
        return $producto;
        $producto = Producto::all();
      
        return view('listarProductoAdmin',compact('producto'));
        //return view('productos.listarProductoAdmin',compact('producto'));
    }
}
