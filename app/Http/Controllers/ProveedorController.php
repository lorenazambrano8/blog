<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    public function index(){
        $proveedores = Proveedor::all();
        return view('proveedores.listarProveedor',compact('proveedores'));
    }
    
    public function create(){
        return view('proveedores.create');
    }

    public function store(Request $request){
        /*return $request->all();*/
        $request->validate([
            'nombre'=>'required',
            'nit'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'correo' =>'required'
        ]);

        $proveedor = new Proveedor();

        $proveedor->nombre = $request->nombre;
        $proveedor->nit = $request->nit;
        $proveedor->direccion = $request->direccion;
        $proveedor->telefono = $request->telefono;
        $proveedor->correo = $request->correo;
    
        $proveedor->save();
        $proveedores = Proveedor::all();
        return view('proveedores.listarProveedor',compact('proveedores'));
        return '<h2>Proveedor Registrado...</h2>';

    }

    public function show(Proveedor $proveedor){
        return view('proveedores.show',compact('proveedor'));
    }

    public function edit(Proveedor $proveedor){
        //$producto = Producto::find($producto);
        return view('proveedores.edit', compact('proveedor'));

    }
    public function update(Request $request, Proveedor $proveedor){
        $proveedor ->nombre = $request->nombre;
        $proveedor ->nit = $request->nit;
        $proveedor ->direccion = $request->direccion;
        $proveedor ->telefono = $request->telefono;
        $proveedor ->correo = $request->correo;
            
        
        $proveedor->save();

        return redirect()->route('proveedores.index', $proveedor);   
    }
    public function destroy(Proveedor $proveedor){
        $proveedor->delete();

        return redirect()->route('proveedores.index');
    }

}
