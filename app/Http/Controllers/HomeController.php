<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke(){
        /*return view('welcome');*/
        return view('home');
    }
 
    // public function variable(Producto $producto){
    //     return view('productos.listarProducto', compact('producto'));
    // }
    
}
