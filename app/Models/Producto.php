<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use App\Models\Cliente;

class Producto extends Model
{
    use HasFactory;

    //protected $guarded = [];
    //Relacion muchos a muchos
    public function cliente(){
        return $this->belongsToMany(Cliente::class);
    }
}
